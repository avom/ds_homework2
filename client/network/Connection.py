import logging
import threading

FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
log = logging.getLogger(__name__)


class Connection:
    '''Manages connection to a server. All RPCs go through here. Also determines if connection to
       server is lost (i.e. server is not reachable or not cooperating anymore).'''

    def __init__(self, proxy, exceptionHandler):
        # Used to make one request at a time as we can't make two concurrent requests.
        self.__lock = threading.Lock()

        self.__proxy = proxy
        self.__exceptionHandler = exceptionHandler
        self.__buffer = ''
        self.__CONNECTION_ERROR_MESSAGE = \
            "Error while talking to server. Assume that connection is lost."
        self.__id = proxy.Handshake()  # TODO: move it out of here
        log.debug('Handshake done. Id is "%s"' % self.__id)
        self.__isAvailable = True

    def SendNickname(self, nickname):
        if not self.__isAvailable:
            return
        self.__lock.acquire()
        try:
            self.__proxy.SetNickname(self.__id, nickname)
        except Exception:
            # Server is written so that it wouldn't throw exceptions. Therefore, if an exception is
            # thrown, there's probably a problem on the server side and there's we'll consider the
            # connection is lost as the session can't be salvaged.
            log.exception(self.__CONNECTION_ERROR_MESSAGE)
            self.__isAvailable = False
        finally:
            self.__lock.release()

    def GetSessions(self):
        if not self.__isAvailable:
            return
        self.__lock.acquire()
        try:
            response = self.__proxy.GetSessions(self.__id)
            tokens = response.split(';')
            result = []
            if tokens[0] != '':
                for token in tokens:
                    result.append(self.__sessionResponseToDict(token))
            return result
        except Exception:
            log.exception(self.__CONNECTION_ERROR_MESSAGE)
            self.__isAvailable = False
        finally:
            self.__lock.release()

    def CreateSession(self, room_size):
        if not self.__isAvailable:
            return
        self.__lock.acquire()
        try:
            response = self.__proxy.CreateSession(self.__id, room_size)
            return self.__sessionResponseToDict(response)
        except Exception:
            log.exception(self.__CONNECTION_ERROR_MESSAGE)
            self.__isAvailable = False
        finally:
            self.__lock.release()

    def JoinSession(self, id):
        if not self.__isAvailable:
            return
        self.__lock.acquire()
        try:
            response = self.__proxy.JoinSession(self.__id, id)
            if response.startswith('join_denied: '):
                return None, response.split()[1]
            return self.__sessionResponseToDict(response), ''
        except Exception:
            log.exception(self.__CONNECTION_ERROR_MESSAGE)
            self.__isAvailable = False
        finally:
            self.__lock.release()

    def LeaveSession(self):
        if not self.__isAvailable:
            return
        self.__lock.acquire()
        try:
            self.__proxy.LeaveSession(self.__id)
        except Exception:
            log.exception(self.__CONNECTION_ERROR_MESSAGE)
            self.__isAvailable = False
        finally:
            self.__lock.release()

    def GetSessionUpdate(self):
        if not self.__isAvailable:
            return
        self.__lock.acquire()
        try:
            session_str = self.__proxy.GetSessionUpdate(self.__id)
            return self.__sessionResponseToDict(session_str) if session_str != '' else None
        except Exception:
            log.exception(self.__CONNECTION_ERROR_MESSAGE)
            self.__isAvailable = False
        finally:
            self.__lock.release()

    def SendAttempt(self, pos, attempt):
        if not self.__isAvailable:
            return
        self.__lock.acquire()
        try:
            value = self.__proxy.Attempt(self.__id, pos, int(attempt))
            log.debug('SendAttempt: %d' % value)
            if value == 0:
                return 'correct'
            elif value == 1:
                return 'wrong'
            else:
                return 'ignored'
        except Exception:
            log.exception(self.__CONNECTION_ERROR_MESSAGE)
            self.__isAvailable = False
        finally:
            self.__lock.release()

    def Close(self):
        if not self.__isAvailable:
            return
        self.__lock.acquire()
        try:
            self.__proxy.Close(self.__id)
        except Exception:
            log.exception(self.__CONNECTION_ERROR_MESSAGE)
        finally:
            self.__isAvailable = False
            self.__lock.release()

    def IsAvailable(self):
        return self.__isAvailable

    def __sessionResponseToDict(self, s):
        id, room_size, player_count, board, players_str, winners_str = s.split('/')
        if players_str != '':
            players = [self.__playerStrToDict(player) for player in players_str.split(',')]
        else:
            players = []
        winners = winners_str.split() if winners_str != '' else None

        return {
            'id': int(id),
            'player_count': int(player_count),
            'room_size': int(room_size),
            'board': board,
            'players': players,
            'winners': winners
        }

    def __playerStrToDict(self, s):
        name, score = s.split(' ')
        return {
            'name': name,
            'score': int(score)
        }
