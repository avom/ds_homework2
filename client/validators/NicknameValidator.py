import logging

FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
log = logging.getLogger(__name__)


class NicknameValidator:
    '''Validates user nickname.'''

    def Validate(self, nickname):
        self.__errorMessage = ''
        MAX_LENGTH = 8
        if nickname is None or nickname == '':
            self.__errorMessage = 'Nickname must not be empty.'
        elif len(nickname) > MAX_LENGTH:
            self.__errorMessage = 'Nickname too long, upto %d characters allowed.' % MAX_LENGTH
        elif not nickname.isalnum():
            self.__errorMessage = 'Nickname can only contain alphanumeric characters.'
        result = self.__errorMessage == ''
        log.debug('Validating nickname "%s" returned %r' % (nickname, result))
        return result

    def GetErrorMessage(self):
        return self.__errorMessage
