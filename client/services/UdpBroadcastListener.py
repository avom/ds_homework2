import logging
import socket


FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
log = logging.getLogger(__name__)


class UdpBroadcastListener:
    '''Listens for UDP broadcast messages from servers and notifies observers when Sudoku Server is 
       found'''

    def __init__(self):
        self.__stopped = False
        self.__observers = []

    def Listen(self, addr):
        msg_prefix = 'SudokuServer@'
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        try:
            sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            sock.bind(addr)
            while not self.__stopped:
                msg, source = sock.recvfrom(1024)
                if not msg.startswith('SudokuServer@'):
                    log.debug('Unknown broadcast message: "%s"' % msg)
                    continue
                addr = msg[len(msg_prefix):]
                host, port = self.__parseHostAndPort(addr)
                if (host is None) or (port is None):
                    log.debug('Invalid host/port in broadcast message: "%s"' % msg)
                    continue
                self.Notify(host, port)
        finally:
            sock.close()

    def Stop(self):
        self.__stopped = True

    def AddObserverFunc(self, func):
        self.__observers.append(func)

    def RemoveObserverFunc(self, func):
        self.__observers.remove(func)

    def Notify(self, host, port):
        for func in self.__observers:
            func(host, port)

    def __parseHostAndPort(self, addr):
        tokens = addr.split(':')
        if len(tokens) != 2:
            return None, None
        host = tokens[0]
        port = self.__parsePort(tokens[1])
        return host, port

    def __parsePort(self, port_str):
        try:
            port = int(port_str)
            if (port >= 0) and (port <= 65535):
                return port
            return None
        except ValueError:
            return None
