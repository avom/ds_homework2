import logging
import threading
import wx
import xmlrpclib

import misc.RuntimeSettings

from misc.ExceptionHandler import ExceptionHandler
from network.Connection import Connection
from services.UdpBroadcastListener import UdpBroadcastListener
from views.ConnectionView import ConnectionView
from views.NickNameView import NickNameView

FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
log = logging.getLogger(__name__)


class LoginService:
    '''Service for establishing a connection and logging into a server.'''

    def __init__(self, guiParent):
        self.__guiParent = guiParent
        self.__serverFinder = UdpBroadcastListener()

    def Login(self):
        nicknameView = NickNameView(self.__guiParent)
        if nicknameView.ShowModal() != wx.ID_OK:
            return None

        connectionView = ConnectionView(self.__guiParent)
        self.__startServerFinder(connectionView.AddServerAsync)
        try:
            while True:
                if connectionView.ShowModal() != wx.ID_OK:
                    return None
                server = connectionView.GetServer()
                proxy = self.__connect(server)  # TODO: remove sock
                if (proxy is None):
                    self.__warnFailedConnection()
                else:
                    break
        finally:
            self.__stopServerFinder()

        self.__nickname = nicknameView.GetNickname()

        exceptionHandler = ExceptionHandler(self.__guiParent)
        connection = Connection(proxy, exceptionHandler)
        connection.SendNickname(self.__nickname)
        return connection

    def GetNickname(self):
        return self.__nickname

    def __connect(self, address):
        try:
            proxy = xmlrpclib.ServerProxy('http://%s:%d' % address)
            return proxy
        except Exception as ex:
            log.error('Cannot connect to %s:%d. Error: %s' % (address + (ex,)))
            return None, None

    def __warnFailedConnection(self):
        dialog = wx.MessageDialog(self.__guiParent,
                                  'Could not establish connection to server.',
                                  'Warning',
                                  wx.OK | wx.ICON_WARNING)
        try:
            dialog.ShowModal()
        finally:
            dialog.Destroy()

    def __startServerFinder(self, on_found_func):
        self.__serverFinder.AddObserverFunc(on_found_func)
        self.__serverFinderThread = \
            threading.Thread(target=self.__serverFinder.Listen,
                             args=(misc.RuntimeSettings.Get('broadcast_addr'),))
        self.__serverFinderThread.start()

    def __stopServerFinder(self):
        self.__serverFinder.Stop()
        self.__serverFinderThread.join()
