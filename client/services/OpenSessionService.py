import logging
import socket
import threading
import time
import wx

from views.SessionListView import SessionListView, SESSION_LIST_VIEW_RESULT_NEW, \
    SESSION_LIST_VIEW_RESULT_OPEN


FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
log = logging.getLogger(__name__)


class OpenSessionService:
    '''Service for creating and/or joining a Sudoku session on the server.'''

    def __init__(self, guiParent, connection):
        self.__guiParent = guiParent
        self.__connection = connection
        self.__listenForSessions = True

    def Open(self):
        socket.setdefaulttimeout(10)  # seconds
        view = SessionListView(self.__guiParent)
        thread = threading.Thread(target=self.__listenSessions, args=(view,))
        thread.start()
        try:
            while self.__connection.IsAvailable():
                dialogResult = view.ShowModal()
                if dialogResult == SESSION_LIST_VIEW_RESULT_NEW:
                    session = self.__connection.CreateSession(view.GetRoomSizeForNewRoom())
                    session = self.__join_session(session)
                    if session is not None:
                        return session
                elif dialogResult == SESSION_LIST_VIEW_RESULT_OPEN:
                    session = view.GetSelectedSession()
                    session = self.__join_session(session)
                    if session is not None:
                        return session
                elif dialogResult == wx.ID_CANCEL:
                    if self.__connection.IsAvailable():
                        return None
                else:
                    log.error("Unknown code from SessionListView.ShowModal(): %d" % dialogResult)

            # If session was established, we would be returned out the function already.
            self.__showMessage('Connection lost!', 'Information', wx.ICON_INFORMATION)
        finally:
            self.__listenForSessions = False
            thread.join()
        return None

    def __join_session(self, session):
        session, denial_reason = self.__connection.JoinSession(session['id'])
        if session is not None:
            return session
        message = 'Request to join session denied: '
        if denial_reason == 'full':
            message += 'session is full.'
        elif denial_reason == 'duplicate_name':
            message += 'user with the same nickname already joined.'
        elif denial_reason in ['no_such_session', 'not_available']:
            message += 'session is not available.'
        self.__showMessage(message, 'Info', wx.ICON_INFORMATION)

    def __showMessage(self, message, title, type):
        dialog = wx.MessageDialog(self.__guiParent, message, title, wx.OK | type)
        try:
            dialog.ShowModal()
        finally:
            dialog.Destroy()

    def __listenSessions(self, view):
        while self.__listenForSessions and self.__connection.IsAvailable():
            sessions = self.__connection.GetSessions()
            wx.CallAfter(view.UpdateSessionsList, sessions)
            time.sleep(1)
        if self.__listenSessions:
            wx.CallAfter(view.Close)
