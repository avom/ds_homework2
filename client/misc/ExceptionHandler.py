import sys
import wx


class ExceptionHandler:
    def __init__(self, guiParent):
        self.__guiParent = guiParent

    def Handle(self, message):
        self.__showMessageAndShutDown(message)

    def __showMessageAndShutDown(self, message):
        message += '\n\nApplication will now close! Sorry!'
        dialog = wx.MessageDialog(self.__guiParent, message, 'Error', wx.OK | wx.ICON_WARNING)
        try:
            dialog.ShowModal()
        finally:
            dialog.Destroy()
        sys.exit(1)
