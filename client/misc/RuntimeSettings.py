'''Provides interface for making settings globally available.'''

__settings = {}


def Get(name):
    if name not in __settings:
        return None
    return __settings[name]


def Set(name, value):
    __settings[name] = value
