import ConfigParser

'''Interface to the settings.ini file for storing application settings and user preferences.'''

__FILENAME = 'settings.ini'
__settings = None
__GENERAL_SECTION = 'General'


def GetNicknameHistory():
    if __settings is None:
        __loadConfig()
    value = __getstring('nickname_history')
    return value.split(',') if value != '' else []


def IncludeNicknameInHistory(nickname):
    history = GetNicknameHistory()
    if nickname in GetNicknameHistory():
        return
    history.append(nickname)
    __setstring('nickname_history', ','.join(history))


def __loadConfig():
    global __settings
    __settings = ConfigParser.ConfigParser()
    __settings.read(__FILENAME)


def __getstring(key):
    if not (__GENERAL_SECTION in __settings.sections()):
        return ''
    return __settings.get(__GENERAL_SECTION, key, '')


def __setstring(key, value):
    if not (__GENERAL_SECTION in __settings.sections()):
        __settings.add_section(__GENERAL_SECTION)
    __settings.set(__GENERAL_SECTION, key, value)
    with open(__FILENAME, 'w+') as f:
        __settings.write(f)
