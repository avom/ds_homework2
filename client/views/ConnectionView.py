import sys
import wx


class ConnectionView(wx.Dialog):
    '''GUI dialog for entering server connection details.'''

    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, title='Choose Sudoku server')

        self.__servers = []

        self.mainSizer = wx.BoxSizer(wx.VERTICAL)

        self.serverListLabel = wx.StaticText(self, label='Available servers')
        self.mainSizer.Add(self.serverListLabel, 0, wx.LEFT | wx.TOP, border=4)

        self.serverListCtrl = wx.ListCtrl(self, -1, style=wx.LC_REPORT)
        self.serverListCtrl.InsertColumn(0, 'Host', wx.LIST_FORMAT_LEFT, width=200)
        self.serverListCtrl.InsertColumn(1, 'Port', wx.LIST_FORMAT_RIGHT, width=100)

        self.mainSizer.Add(self.serverListCtrl, 0,
                           wx.LEFT | wx.TOP | wx.RIGHT | wx.EXPAND | wx.FIXED_MINSIZE,
                           border=4)

        self.joinButton = wx.Button(self, wx.ID_OK, 'Join')
        self.cancelButton = wx.Button(self, wx.ID_CANCEL, 'Cancel')

        self.buttonsSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.buttonsSizer.Add(self.joinButton, flag=wx.RIGHT, border=4)
        self.buttonsSizer.Add(self.cancelButton)
        self.mainSizer.Add(self.buttonsSizer, 0, wx.ALL | wx.EXPAND, border=4)

        self.SetSizer(self.mainSizer)
        self.mainSizer.Fit(self)

        self.Bind(wx.EVT_BUTTON, self.OnJoinClick, self.joinButton)

    def OnJoinClick(self, event):
        if self.serverListCtrl.GetFirstSelected() >= 0:
            event.Skip()
        else:
            self.__showWarningMessage('Select a server first!')

    def GetServer(self):
        index = self.serverListCtrl.GetFirstSelected()
        return self.__servers[index] if index >= 0 else None

    def AddServerAsync(self, host, port):
        if self:
            wx.CallAfter(self.__addServer, (host, port))

    def __addServer(self, addr):
        if addr in self.__servers:
            return
        self.__servers.append(addr)
        host, port = addr
        index = self.serverListCtrl.InsertStringItem(sys.maxint, host)
        self.serverListCtrl.SetStringItem(index, 1, str(port))
        # TODO: remove servers that stop broadcasting

    # TODO: Move to utils
    def __showWarningMessage(self, message):
        dialog = wx.MessageDialog(self, message, 'Warning', wx.OK | wx.ICON_WARNING)
        try:
            dialog.ShowModal()
        finally:
            dialog.Destroy()
