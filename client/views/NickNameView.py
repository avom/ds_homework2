import logging
import wx

import misc.Settings

from validators.NicknameValidator import NicknameValidator

FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
log = logging.getLogger(__name__)


class NickNameView(wx.Dialog):
    '''GUI dialog for asking nickname from the user.'''

    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, title='Enter Nickname')

        self.nicknameLabel = wx.StaticText(self, label='Nickname')
        self.nicknameCombo = wx.ComboBox(self,
                                         choices=misc.Settings.GetNicknameHistory(),
                                         style=wx.CB_DROPDOWN)
        self.nicknameCombo.SetMaxLength(8)
        self.okButton = wx.Button(self, wx.ID_OK, 'OK')
        self.cancelButton = wx.Button(self, wx.ID_CANCEL, 'Cancel')

        self.nicknameSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.nicknameSizer.Add(self.nicknameLabel, flag=wx.RIGHT, border=4)
        self.nicknameSizer.Add(self.nicknameCombo, proportion=1, border=4)

        self.buttonsSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.buttonsSizer.Add((0, 0), 1, wx.EXPAND | wx.LEFT, border=4)
        self.buttonsSizer.Add(self.okButton, flag=wx.RIGHT, border=4)
        self.buttonsSizer.Add(self.cancelButton, flag=wx.RIGHT, border=4)

        self.mainSizer = wx.BoxSizer(wx.VERTICAL)
        self.mainSizer.Add(self.nicknameSizer,
                           flag=wx.LEFT | wx.RIGHT | wx.EXPAND | wx.TOP,
                           border=4)
        self.mainSizer.Add((-1, 4))
        self.mainSizer.Add(self.buttonsSizer, 1, wx.EXPAND)
        self.mainSizer.Add((-1, 4))

        self.SetSizer(self.mainSizer)
        self.mainSizer.Fit(self)

        self.Bind(wx.EVT_BUTTON, self.OnOkClick, self.okButton)

    def OnOkClick(self, event):
        nickname = self.nicknameCombo.GetValue()
        validator = NicknameValidator()
        if validator.Validate(nickname):
            misc.Settings.IncludeNicknameInHistory(nickname)
            event.Skip()
        else:
            self.__showWarningMessage(validator.GetErrorMessage())

    def GetNickname(self):
        return self.nicknameCombo.GetValue()

    def __showWarningMessage(self, message):
        dialog = wx.MessageDialog(self, message, 'Warning', wx.OK | wx.ICON_WARNING)
        try:
            dialog.ShowModal()
        finally:
            dialog.Destroy()
