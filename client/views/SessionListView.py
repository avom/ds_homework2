import sys
import wx


SESSION_LIST_VIEW_RESULT_NEW = 1
SESSION_LIST_VIEW_RESULT_OPEN = 2


class SessionListView(wx.Dialog):
    '''GUI dialog which presents a list of open sessions for joining or allows creating a new
       session.'''

    def __init__(self, parent):
        wx.Dialog.__init__(self, parent, title='Multiplayer Game')

        self.__sessions = []

        self.mainSizer = wx.BoxSizer(wx.VERTICAL)

        self.sessionLabel = wx.StaticText(self, label='Available sessions')
        self.mainSizer.Add(self.sessionLabel, 0, wx.LEFT | wx.TOP, border=4)

        self.sessionListCtrl = wx.ListCtrl(self, -1, style=wx.LC_REPORT)
        self.sessionListCtrl.InsertColumn(0, 'Session', wx.LIST_FORMAT_LEFT, width=100)
        self.sessionListCtrl.InsertColumn(1, 'Room size', wx.LIST_FORMAT_RIGHT, width=100)
        self.sessionListCtrl.InsertColumn(2, 'Players', wx.LIST_FORMAT_RIGHT, width=100)

        self.mainSizer.Add(self.sessionListCtrl, 0,
                           wx.LEFT | wx.TOP | wx.RIGHT | wx.EXPAND | wx.FIXED_MINSIZE,
                           border=4)

        self.newSessionButton = wx.Button(self, -1, 'New')
        self.openSessionButton = wx.Button(self, -1, 'Join')

        self.buttonsSizer = wx.BoxSizer(wx.HORIZONTAL)
        self.buttonsSizer.Add(self.newSessionButton, flag=wx.RIGHT, border=4)
        self.buttonsSizer.Add(self.openSessionButton)
        self.mainSizer.Add(self.buttonsSizer, 0, wx.ALL | wx.EXPAND, border=4)

        self.SetSizer(self.mainSizer)
        self.mainSizer.Fit(self)

        self.Bind(wx.EVT_BUTTON, self.OnNewSessionClick, self.newSessionButton)
        self.Bind(wx.EVT_BUTTON, self.OnOpenSessionClick, self.openSessionButton)

    def GetSelectedSession(self):
        index = self.sessionListCtrl.GetFirstSelected()
        if index < 0:
            return None
        i = self.sessionListCtrl.GetItem(index).GetData()
        return self.__sessions[i]

    def GetRoomSizeForNewRoom(self):
        return self.__roomSize

    def OnNewSessionClick(self, event):
        dlg = wx.TextEntryDialog(self, 'Room Size', 'Create New Session')
        try:
            while True:
                if dlg.ShowModal() == wx.ID_OK:
                    value = dlg.GetValue()
                    room_size = self.__parseRoomSize(value)
                    if room_size is None or room_size <= 0:
                        self.__showWarningMessage('Room size must be a positive integer.')
                    else:
                        self.__roomSize = room_size
                        self.EndModal(SESSION_LIST_VIEW_RESULT_NEW)
                        break
                else:
                    break
        finally:
            dlg.Destroy()

    def OnOpenSessionClick(self, event):
        if self.sessionListCtrl.GetFirstSelected() >= 0:
            self.EndModal(SESSION_LIST_VIEW_RESULT_OPEN)

    def __parseRoomSize(self, s):
        if s.isdigit():
            return int(s)
        return None

    def __sesssionsToListOfStrings(self, sessions):
        result = []
        for session in sessions:
            result.append('%s/%s/%s' % (session['id'],
                                        session['player_count'],
                                        session['room_size']))
        return result

    # TODO: Move to utils
    def __showWarningMessage(self, message):
        dialog = wx.MessageDialog(self, message, 'Warning', wx.OK | wx.ICON_WARNING)
        try:
            dialog.ShowModal()
        finally:
            dialog.Destroy()

    def UpdateSessionsList(self, sessions):
        selected = self.GetSelectedSession()
        self.sessionListCtrl.DeleteAllItems()
        for i in range(len(sessions)):
            session = sessions[i]
            index = self.sessionListCtrl.InsertStringItem(sys.maxint, 'Room ' + str(session['id']))
            self.sessionListCtrl.SetStringItem(index, 1, str(session['room_size']))
            self.sessionListCtrl.SetStringItem(index, 2, str(session['player_count']))
            self.sessionListCtrl.SetItemData(index, i)

            if (selected is not None) and (session['id'] == selected['id']):
                self.sessionListCtrl.Select(index)
        self.__sessions = sessions
