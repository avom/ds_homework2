import sys
import threading
import time
import wx

from services.LoginService import LoginService
from services.OpenSessionService import OpenSessionService

from BoardView import BoardView


class MainView(wx.Frame):
    '''GUI dialog containing Sudoku board and user scores.'''

    def __init__(self, parent):
        wx.Frame.__init__(self, parent, title='Sudoku', size=(60 * 9 + 200 + 12, 60 * 9 + 8))

        self.__connectionWaitThread = None
        self.__session = None

        self.board = BoardView(self)
        self.playersListCtrl = wx.ListCtrl(self, -1, style=wx.LC_REPORT)
        self.playersListCtrl.InsertColumn(0, 'Player', wx.LIST_FORMAT_LEFT, width=150)
        self.playersListCtrl.InsertColumn(1, 'Score', wx.LIST_FORMAT_RIGHT, width=50)

        self.statusbar = self.CreateStatusBar(1)

        self.sizer = wx.BoxSizer(wx.HORIZONTAL)
        self.sizer.Add(self.board, 1, flag=wx.ALL | wx.EXPAND, border=4)
        self.sizer.Add(self.playersListCtrl, 0, flag=wx.TOP | wx.RIGHT | wx.BOTTOM | wx.EXPAND,
                       border=4)
        self.SetSizer(self.sizer)

        self.Bind(wx.EVT_SHOW, self.OnOpen, self)
        self.Bind(wx.EVT_CLOSE, self.OnClose, self)
        self.Bind(wx.EVT_CHAR_HOOK, self.OnKeyDown)

    def OnOpen(self, event):
        if not self.__login():
            self.Close()
        elif not self.__openSession():
            self.Close()

    def OnClose(self, event):
        if self.__connection is not None and self.__connection.IsAvailable():
            if self.__session is not None:        # HACK: If there's no session, then the view was
                self.__connection.LeaveSession()  # never opened meaning that a user descided not to
                if self.__openSession():          # open a session, so there's no reason to ask
                    return                        # again. Fix it!
            if self.__connection is not None:
                self.__connection.Close()
        event.Skip()

    def OnKeyDown(self, event):
        keycode = event.GetKeyCode()
        attempt = self.__getNumberAttempted(keycode)
        if attempt is not None:
            response = self.__connection.SendAttempt(self.board.GetSelectedCell(), str(attempt))
            if response == 'ignored':
                self.statusbar.SetStatusText('Your guess ' + str(attempt) + ' was ignored, ' +
                                             'because someone already guessed the correct value!')
            else:
                self.statusbar.SetStatusText('Your guess %d was %s.' % (attempt, response))

    def __updateGameState(self, session):
        self.__session = session
        self.board.SetState(self.__session['board'])
        self.__updatePlayers()

        if self.__session['winners'] is not None:
            self.__showWinners()
            self.Close()

    def __updatePlayers(self):
        # TODO: update without deleting everything
        self.playersListCtrl.DeleteAllItems()
        for player in self.__session['players']:
            index = self.playersListCtrl.InsertStringItem(sys.maxint, player['name'])
            self.playersListCtrl.SetStringItem(index, 1, str(player['score']))

    def __showWinners(self):
        winners = self.__session['winners']
        if len(winners) == 1:
            if winners[0] == self.__nickname:
                message = 'You win!'
            else:
                message = 'Player %s won!' % winners[0]
        else:
            other_winners = list(winners)
            if self.__nickname in winners:
                message = 'You together with '
                other_winners.remove(self.__nickname)
            else:
                message = 'Players '
            message += ','.join(other_winners) + ' won!'
        self.__showMessage(message, 'Information', wx.ICON_INFORMATION)

    def __getNumberAttempted(self, keycode):
        attempt = keycode - ord('0')
        if attempt <= 0 or attempt > 9:
            attempt = keycode - wx.WXK_NUMPAD0

        if attempt >= 1 and attempt <= 9:
            return attempt
        return None

    def __login(self):
        loginService = LoginService(self)
        self.__connection = loginService.Login()
        if self.__connection is None:
            self.Close()
            return False
        self.__nickname = loginService.GetNickname()
        return True

    def __openSession(self):
        session = OpenSessionService(self, self.__connection).Open()
        if session is None:
            return False
        self.__updateGameState(session)

        self.__connectionWaitThread = threading.Thread(target=self.__listenGameState)
        self.__connectionWaitThread.start()

        return True

    def __listenGameState(self):
        game_over = False
        while not game_over and self.__connection.IsAvailable():
            session = self.__connection.GetSessionUpdate()
            if session is not None:
                game_over = session['winners'] is not None
                wx.CallAfter(self.__updateGameState, session)
            time.sleep(0.05)  # guarantee that main thread can use connection
        if not self.__connection.IsAvailable():
            wx.CallAfter(self.__closeOnConnectionLost)

    def __closeOnConnectionLost(self):
        self.__showMessage('Connection lost!', 'Warning', wx.ICON_WARNING)
        self.Close()

    def __showMessage(self, message, title, type):
        dialog = wx.MessageDialog(self, message, title, wx.OK | type)
        try:
            dialog.ShowModal()
        finally:
            dialog.Destroy()
