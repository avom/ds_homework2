import wx


class BoardView(wx.Panel):
    '''GUI component for presented state of a Sudoku puzzle.'''

    def __init__(self, parent):
        super(BoardView, self).__init__(parent)

        self.__BOX_BORDER_WIDTH = 6
        self.__CELL_BORDER_WIDTH = 3

        self.__state = ''.join([' ' for i in range(9 * 9)])
        self.__selectedRow = None
        self.__selectedCol = None

        self.SetBackgroundStyle(wx.BG_STYLE_CUSTOM)

        self.Bind(wx.EVT_SIZE, self.OnSize)
        self.Bind(wx.EVT_PAINT, self.OnPaint)
        self.Bind(wx.EVT_LEFT_DOWN, self.OnMouseLeftDown)

    def SetState(self, state):
        # TODO: validate state
        self.__state = state
        self.Refresh()

    def GetSelectedCell(self):
        return self.__selectedRow, self.__selectedCol

    def OnSize(self, event):
        event.Skip()
        self.Refresh()

    def OnMouseLeftDown(self, event):
        event.Skip()
        x, y = event.GetPosition()
        width, height = self.GetClientSize()
        self.__selectedCol = 9 * x / width
        self.__selectedRow = 9 * y / height
        self.Refresh()

    def OnPaint(self, event):
        width, height = self.GetClientSize()
        dc = wx.AutoBufferedPaintDC(self)
        dc.Clear()
        dc.SetPen(wx.Pen(wx.WHITE, 0))
        dc.SetBrush(wx.Brush(wx.WHITE))
        dc.DrawRectangle(0, 0, width, height)

        if self.__state != '':
            self.__drawBoxBorders(dc, width, height)
            self.__drawNumbers(dc, width, height)
            self.__drawSelectedCell(dc)
        else:
            self.__drawWaitingForPlayersMessage(dc, width, height)

    def __drawBoxBorders(self, dc, width, height):
        total_border_width = 4 * self.__BOX_BORDER_WIDTH + 3 * 2 * self.__CELL_BORDER_WIDTH
        cell_width = (width - total_border_width) / 9
        cell_height = (height - total_border_width) / 9
        extra_width = width - total_border_width - 9 * cell_width
        extra_height = height - total_border_width - 9 * cell_height

        dc.SetBrush(wx.Brush(wx.BLACK))
        dc.SetPen(wx.Pen(wx.BLACK, 1))

        self.__rowCenters = []
        self.__colCenters = []
        top = 0
        left = 0
        for i in range(10):
            w = self.__BOX_BORDER_WIDTH if i % 3 == 0 else self.__CELL_BORDER_WIDTH
            dc.DrawRectangle(left, 0, w, height)
            dc.DrawRectangle(0, top, width, w)
            top += w + cell_height
            left += w + cell_width
            self.__rowCenters.append(top - cell_height / 2)
            self.__colCenters.append(left - cell_width / 2)
            if extra_width > 0:
                extra_width -= 1
                left += 1
            if extra_height > 0:
                extra_height -= 1
                top += 1

    def __drawNumbers(self, dc, width, height):
        col_width = width / 9
        row_height = height / 9
        dc.SetFont(wx.Font(min(row_height, col_width) / 2, wx.DEFAULT, wx.NORMAL, wx.NORMAL))
        dc.SetTextForeground(wx.BLACK)
        for i in range(len(self.__state)):
            value = self.__state[i]
            row = i / 9
            col = i % 9
            w, h = dc.GetTextExtent(value)
            center_x = self.__colCenters[col]
            center_y = self.__rowCenters[row]
            dc.DrawText(value, center_x - w / 2, center_y - h / 2)

    def __drawSelectedCell(self, dc):
        if self.__selectedCol is None or self.__selectedRow is None:
            return

        dc.SetBrush(wx.Brush('GRAY'))
        i = self.__selectedRow * 9 + self.__selectedCol
        if self.__state[i] == ' ':
            x = self.__colCenters[self.__selectedCol]
            y = self.__rowCenters[self.__selectedRow]
            dc.FloodFill(x, y, wx.BLACK, wx.FLOOD_BORDER)

    def __drawWaitingForPlayersMessage(self, dc, width, height):
        message = "Waiting for players..."
        col_width = width / len(message)
        row_height = height / len(message)
        dc.SetFont(wx.Font(min(row_height, col_width) / 2, wx.DEFAULT, wx.NORMAL, wx.NORMAL))
        w, h = dc.GetTextExtent(message)
        dc.DrawText(message, (width - w) / 2, (height - h) / 2)
