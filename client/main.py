import wx

from argparse import ArgumentParser

import misc.RuntimeSettings

from views.MainView import MainView


DEFAULT_BROADCAST_PORT = 12345


if __name__ == '__main__':
    parser = ArgumentParser(description='Sudoku Server',
                            version='0.1')
    parser.add_argument('-b', '--broadcast-host',
                        help='UDP broadcast host address, defaults to "" (all interfaces)',
                        default='')
    parser.add_argument('-p', '--broadcast-port', type=int,
                        help='UDP broadcast host port, defaults to %d' % DEFAULT_BROADCAST_PORT,
                        default=DEFAULT_BROADCAST_PORT)
    args = parser.parse_args()

    misc.RuntimeSettings.Set('broadcast_addr', ('', args.broadcast_port))

    app = wx.App()
    mainform = MainView(None)
    mainform.Show()
    app.MainLoop()
