import copy


class Sudoku:
    '''Manages state of Sudoku puzzle'''

    def __init__(self, puzzle, solution):
        self.__puzzle = puzzle
        self.__solution = solution

    def GetPuzzleState(self):
        return copy.deepcopy(self.__puzzle)

    def GuessCell(self, row, col, value):
        if self.__solution[row][col] == value:
            self.__puzzle[row][col] = value
            return True
        return False

    def IsCellSolved(self, row, col):
        return self.__puzzle[row][col] == self.__solution[row][col]

    def IsGameOver(self):
        for row in self.__puzzle:
            for cell in row:
                if cell == 0:
                    return False
        return True
