import copy
import logging
import random
import time

from Sudoku import Sudoku

FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
log = logging.getLogger(__name__)


class SudokuGenerator:
    '''Generates a Sudoku puzzle with single unique solution using a following algorithm.
       1) First generates a random solution to empyt board.
       2) Starting with a full solution starts removing numbers randomly so that there's only one
          single solution.
       3) When reaching a state where none of the remaining numbers can be removed (while keeping
          a solution unique). It stops and returns the puzzle.
       There's 1 second soft time limit when generation. This makes creating new session faster, but
       eliminates hardest possible problems.'''

    def __init__(self):
        self.__size = 9
        self.__timelimit_sec = 1

    def Generate(self):
        log.info('Generating Sudoku puzzle.')
        self.__numberInRow = self.__generateFlagMatrix()
        self.__numberInColumn = self.__generateFlagMatrix()
        self.__numberInBox = self.__generateFlagMatrix()

        self.__uniquenessCheck = False
        empty_board = [[0 for col in range(self.__size)] for row in range(self.__size)]
        solution = self.__solve(empty_board)
        log.info('Sudoku solution generated.')
        puzzle = self.__emptyCellsUntilUniquelySolvable(copy.deepcopy(solution))
        log.info('Sudoku puzzle generated.')
        return Sudoku(puzzle, solution)

    def __generateFlagMatrix(self):
        result = []
        for i in range(self.__size):
            # index 0 is not necessary, but makes code simpler
            nums = [False for num in range(0, self.__size + 1)]
            result.append(nums)
        return result

    def __solve(self, board, check_unique_solution=False):
        self.__check_unique_solution = check_unique_solution
        self.__solutions_found = 0
        self.__inc = 0
        return self.__backtracking(0, 0, board)

    def __backtracking(self, row, col, board):
        if row >= self.__size:
            self.__solutions_found += 1
            if not self.__check_unique_solution or self.__solutions_found > 1:
                return copy.deepcopy(board)
            return None

        next_col = col + 1 if col + 1 < self.__size else 0
        next_row = row if next_col > 0 else row + 1
        if board[row][col] != 0:
            return self.__backtracking(next_row, next_col, board)

        box = 3 * (row // 3) + col // 3
        nums = list(range(1, self.__size + 1))
        random.shuffle(nums)
        for num in nums:
            if self.__numberInRow[row][num]:
                continue
            if self.__numberInColumn[col][num]:
                continue
            if self.__numberInBox[box][num]:
                continue

            self.__numberInRow[row][num] = True
            self.__numberInColumn[col][num] = True
            self.__numberInBox[box][num] = True

            board[row][col] = num
            result = self.__backtracking(next_row, next_col, board)
            board[row][col] = 0

            # Restore board and __numberIn* tables, when we're testing for unique solution.
            if not self.__check_unique_solution and result is not None:
                return result

            self.__numberInRow[row][num] = False
            self.__numberInColumn[col][num] = False
            self.__numberInBox[box][num] = False

            if result is not None:
                return result
        return None

    def __emptyCellsUntilUniquelySolvable(self, board):
        start_time = time.time()
        empty_cell_count = 0
        while (self.__solutions_found <= 1) and (time.time() - start_time < self.__timelimit_sec):
            empty_cell_count += 1
            filled_cells = self.__getShuffledFilledCells(board)
            for cell in filled_cells:
                row, col = cell
                box = 3 * (row // 3) + col // 3
                num = board[row][col]

                self.__numberInRow[row][num] = False
                self.__numberInColumn[col][num] = False
                self.__numberInBox[box][num] = False

                board[row][col] = 0
                self.__solve(board, check_unique_solution=True)
                if self.__solutions_found == 1:
                    result = copy.deepcopy(board)
                    log.debug('Removed %d cells, still having unique solution.' % empty_cell_count)
                    break
                board[row][col] = num

                self.__numberInRow[row][num] = True
                self.__numberInColumn[col][num] = True
                self.__numberInBox[box][num] = True
        log.info('Found a puzzle with %d empty cells.' % (empty_cell_count - 1))
        return result

    def __getShuffledFilledCells(self, board):
        result = []
        for row in range(self.__size):
            for col in range(self.__size):
                if board[row][col] != 0:
                    result.append((row, col))
        random.shuffle(result)
        return result
