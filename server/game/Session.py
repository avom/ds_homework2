import copy
import logging
import threading

FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
log = logging.getLogger(__name__)

SUDOKU_ATTEMPT_CORRECT = 0
SUDOKU_ATTEMPT_WRONG = 1
SUDOKU_ATTEMPT_IGNORED = 2


class Session:
    '''Manages session state: sudoku, room size and players.'''

    def __init__(self, id, room_size, sudoku):
        self.__lock = threading.Lock()
        self.__id = id
        self.__room_size = room_size
        self.__players = {}
        self.__sudoku = sudoku
        self.__game_started = False
        self.__game_over = False
        self.__winner = None
        self.__listeners = []

    def GetId(self):
        self.__lock.acquire()
        try:
            return self.__id
        finally:
            self.__lock.release()

    def GetRoomSize(self):
        self.__lock.acquire()
        try:
            return self.__room_size
        finally:
            self.__lock.release()

    def GetPlayerCount(self):
        self.__lock.acquire()
        try:
            return len(self.__players)
        finally:
            self.__lock.release()

    def GetSudoku(self):
        self.__lock.acquire()
        try:
            return self.__sudoku
        finally:
            self.__lock.release()

    def GetScores(self):
        self.__lock.acquire()
        try:
            return copy.deepcopy(self.__players)
        finally:
            self.__lock.release()

    def AddPlayer(self, player):
        self.__lock.acquire()
        try:
            if self.__game_over:
                return False, 'not_available'
            if self.__isFull():
                return False, 'full'
            if player in self.__players:
                return False, 'duplicate_name'

            self.__players[player] = 0
            if len(self.__players) == self.__room_size:
                self.__game_started = True
            self.__notifyListeners('new_player')
            return True, ''
        finally:
            self.__lock.release()

    def RemovePlayer(self, player):
        log.info('Remove')
        self.__lock.acquire()
        try:
            del self.__players[player]
            if len(self.__players) <= 1:
                self.__game_over = True
            self.__notifyListeners('player_removed')
        finally:
            self.__lock.release()

    def Attempt(self, player, pos, value):
        self.__lock.acquire()
        try:
            if self.__sudoku.IsCellSolved(pos[0], pos[1]):
                return SUDOKU_ATTEMPT_IGNORED
            elif self.__sudoku.GuessCell(pos[0], pos[1], value):
                if player in self.__players:
                    self.__players[player] += 1
                self.__game_over = self.__sudoku.IsGameOver()
                self.__notifyListeners('correct_attempt')
                return SUDOKU_ATTEMPT_CORRECT
            else:
                if player in self.__players:
                    self.__players[player] -= 1
                self.__notifyListeners('incorrect_attempt')
                return SUDOKU_ATTEMPT_WRONG
        finally:
            self.__lock.release()

    def IsGameStarted(self):
        self.__lock.acquire()
        try:
            return self.__game_started
        finally:
            self.__lock.release()

    def IsGameOver(self):
        self.__lock.acquire()
        try:
            return self.__game_over
        finally:
            self.__lock.release()

    def Winners(self):
        self.__lock.acquire()
        try:
            return self.__getWinner()
        finally:
            self.__lock.release()

    def GetStateAsString(self):
        self.__lock.acquire()
        try:
            board = self.__sudoku.GetPuzzleState()
            board_str = ''
            if self.__game_started:
                for row in board:
                    board_str += ''.join([str(value) if value > 0 else ' ' for value in row])

            scores = self.__players.iteritems()
            score_str = ','.join([name + ' ' + str(value) for name, value in scores])

            winners = ' '.join(self.__getWinners())

            result = '%d/%d/%d' % (self.__id, self.__room_size, len(self.__players))
            result += '/' + board_str + '/' + score_str + '/' + winners
            return result
        finally:
            self.__lock.release()

    def AddListener(self, listener):
        self.__lock.acquire()
        try:
            self.__listeners.append(listener)
        finally:
            self.__lock.release()

    def RemoveListener(self, listener):
        self.__lock.acquire()
        try:
            self.__listeners.remove(listener)
        finally:
            self.__lock.release()

    def __isFull(self):
        return len(self.__players) == self.__room_size

    def __getPlayersWithHighestScore(self):
        highest_score = self.__getHighestScore()
        for player in self.__players:
            if highest_score == self.__players[player]:
                yield player

    def __getHighestScore(self):
        result = None
        for player in self.__players:
            score = self.__players[player]
            if result is None or result < score:
                result = score
        return result

    def __getWinners(self):
        if not self.__game_over:
            return []
        return list(self.__getPlayersWithHighestScore())

    def __notifyListeners(self, event_name):
        for listener in self.__listeners:
            listener(event_name)
