import logging
import threading
import uuid

from SimpleXMLRPCServer import SimpleXMLRPCServer, SimpleXMLRPCRequestHandler
from SocketServer import ThreadingMixIn

from services.ClientService import ClientService
from services.SessionsService import SessionsService


FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
log = logging.getLogger(__name__)


CLIENT_OFFLINE_THRESHOLD_SECONDS = 60


class SimpleThreadedXMLRPCServer(ThreadingMixIn, SimpleXMLRPCServer):
    '''Helper class for making RPC server multithreaded.'''
    pass


class ServerRequestHandler(SimpleXMLRPCRequestHandler):
    '''Custom request handler for RPC Server.'''
    rpc_path = ('/RPC2')


class Server:
    '''Handles incoming RPC calls. 
       Creates ClientService instances on first connection and directs incoming RPC calls to 
       corresponding ClientServices.'''
    def __init__(self, addr):
        self.__rpc_server = SimpleThreadedXMLRPCServer(addr,
                                                       requestHandler=ServerRequestHandler,
                                                       logRequests=False)
        self.__rpc_server.register_introspection_functions()  # TODO: only register specific funcs
        self.__rpc_server.register_instance(self)

        self.__sessionsService = SessionsService()
        self.__clients = {}

        self.__connectedClients = set()
        self.__activeClients = set()

        self.__stopped = False

    def Start(self):
        log.info('RPC Server started.')
        try:
            self.__startCleanupTimer()
            self.__startStopCheckTimer()
            self.__rpc_server.serve_forever()
        except KeyboardInterrupt:
            log.info('CTRL+C received.')
        finally:
            self.__stopped = True
            log.info('RPC Server stopped.')

    def Handshake(self):
        '''Sets up a client session, by creating a ClientService instance for servicing calling
           client and returning a random ID specific to that client for later identification.'''
        try:
            client_id = uuid.uuid4().hex
            log.debug('Received handshake, setting client_id to "%s"' % client_id)

            self.__activeClients.add(client_id)
            client = ClientService(self.__sessionsService, self, client_id)
            self.__clients[client_id] = client
            threading.Thread(target=client.Run).start()

            return client_id
        except Exception as ex:
            log.exception('Exception during RPC request "Handshake"')

    def SetNickname(self, client_id, nickname):
        self.__activeClients.add(client_id)
        try:
            log.debug('SetNickname("%s", "%s")' % (client_id, nickname))
            self.__clients[client_id].SetNickname(nickname)
            return ''
        except Exception as ex:
            log.exception('Exception during RPC request "SetNickname"')

    def GetSessions(self, client_id):
        self.__activeClients.add(client_id)
        try:
            sessions = self.__clients[client_id].GetSessions()
            result = ';'.join([session.GetStateAsString() for session in sessions])
            return result
        except Exception as ex:
            log.exception('Exception during RPC request "GetSessions"')

    def CreateSession(self, client_id, room_size):
        self.__activeClients.add(client_id)
        try:
            log.debug('CreateSession("%s", %d)' % (client_id, room_size))
            session = self.__clients[client_id].CreateSession(room_size)
            return session.GetStateAsString()
        except Exception as ex:
            log.exception('Exception during RPC request "CreateSession"')

    def JoinSession(self, client_id, session_id):
        self.__activeClients.add(client_id)
        try:
            log.debug('JoinSession("%s", %d)' % (client_id, session_id))
            session, reason = self.__clients[client_id].JoinSession(session_id)
            if session is None:
                return 'join_denied: ' + reason
            else:
                return session.GetStateAsString()
        except Exception as ex:
            log.exception('Exception during RPC request "JoinSession"')

    def LeaveSession(self, client_id):
        self.__activeClients.add(client_id)
        try:
            log.debug('LeaveSession("%s")' % (client_id))
            self.__clients[client_id].RemoveClientFromSession()
            return ''
        except Exception as ex:
            log.exception('Exception during RPC request "LeaveSession"')

    def Attempt(self, client_id, pos, value):
        self.__activeClients.add(client_id)
        try:
            log.debug('Attempt("%s", %d, %d, %d)' % ((client_id,) + tuple(pos) + (value,)))
            return self.__clients[client_id].Attempt(pos, value)
        except Exception as ex:
            log.exception('Exception during RPC request "Attempt"')

    def Close(self, client_id):
        try:
            log.debug('Close("%s")' % (client_id))
            self.__clients[client_id].Stop()
            del self.__clients[client_id]
            return ''
        except Exception as ex:
            log.exception('Exception during RPC request "Close"')

    def GetSessionUpdate(self, client_id):
        self.__activeClients.add(client_id)
        session = self.__clients[client_id].GetSessionUpdate()
        return session.GetStateAsString() if session is not None else ''

    def __cleanupOfflineClients(self):
        if self.__stopped:
            return  # No reason to clean anymore. System is shutting down anyway.

        offlineClients = self.__connectedClients - self.__activeClients
        self.__connectedClients = self.__activeClients
        self.__activeClients = set()
        for client_id in offlineClients:
            if client_id in self.__clients:
                self.__clients[client_id].Stop()
                del self.__clients[client_id]

        if not self.__stopped:
            self.__startCleanupTimer()
        log.info('Done cleaning up clients who lost connection: %s' %
                 ', '.join(list(offlineClients)))

    def __startCleanupTimer(self):
        threading.Timer(CLIENT_OFFLINE_THRESHOLD_SECONDS, self.__cleanupOfflineClients)

    def __startStopCheckTimer(self):
        if not self.__stopped:
            threading.Timer(1, self.__startStopCheckTimer)
