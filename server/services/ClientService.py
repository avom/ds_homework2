import logging
import time


FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
log = logging.getLogger(__name__)


class ClientService:
    '''Each instance manages a single client. It handles messages coming from clients and starts
       actions based on client requests.'''

    def __init__(self, sessionsService, rpc_server, client_id):
        self.__sessionsService = sessionsService
        self.__rpc_server = rpc_server
        self.__client_id = client_id
        self.__nickname = None
        self.__session = None
        self.__CPU_THROTTLING = 0.01

    def Run(self):
        log.info('Client %s connected.' % self.__client_id)
        try:
            self.__stopped = False
            self.__waitNickname()
            self.__sessionUpdateNeeded = False
            while not self.__stopped:
                time.sleep(self.__CPU_THROTTLING)  # CPU throttling
        except Exception as ex:
            log.exception('ClientService for "%s" stopped due to error.' % self.__client_id)
        finally:
            self.RemoveClientFromSession()

    def SetNickname(self, nickname):
        self.__nickname = nickname

    def GetSessions(self):
        return self.__sessionsService.GetSessions()

    def CreateSession(self, room_size):
        return self.__sessionsService.CreateSession(room_size)

    def JoinSession(self, session_id):
        session = self.__sessionsService.GetSession(session_id)
        if session is None:
            return None, 'no_such_session'
        accepted, reason = session.AddPlayer(self.__nickname)
        if accepted:
            session.AddListener(self.__sessionListener)
            self.__session = session
            return session, ''
        else:
            return None, reason

    def Attempt(self, pos, value):
        return self.__session.Attempt(self.__nickname, pos, value)

    def RemoveClientFromSession(self):
        if self.__session is None:
            return
        self.__session.RemoveListener(self.__sessionListener)
        self.__session.RemovePlayer(self.__nickname)
        self.__session = None

    def GetSessionUpdate(self):
        if self.__sessionUpdateNeeded:
            self.__sessionUpdateNeeded = False
            return self.__session
        else:
            return None

    def Stop(self):
        self.RemoveClientFromSession()
        self.__stopped = True

    def __waitNickname(self):
        while (not self.__stopped) and (self.__nickname is None):
            time.sleep(self.__CPU_THROTTLING)  # CPU throttling

    def __sessionListener(self, event_name):
        # We only care if update is needed. Specific event type doesn't matter as the notification
        # messages contains everything needed anyway.
        self.__sessionUpdateNeeded = True
