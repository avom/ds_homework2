import threading

from game.Session import Session
from game.SudokuGenerator import SudokuGenerator


class SessionsService:
    '''Creates and manages Sudoku solving sessions.'''

    def __init__(self):
        self.__lock = threading.Lock()
        self.__sessions = {}
        self.__nextId = 1
        self.__sudokuGenerator = SudokuGenerator()

    def GetSession(self, session_id):
        self.__lock.acquire()
        try:
            if session_id in self.__sessions:
                return self.__sessions[session_id]
        finally:
            self.__lock.release()

    def GetSessions(self):
        self.__lock.acquire()
        try:
            self.__removeFinishedSessions()
            return self.__sessions.values()
        finally:
            self.__lock.release()

    def CreateSession(self, game_room_size):
        self.__lock.acquire()
        try:
            sudoku = self.__sudokuGenerator.Generate()
            session = Session(self.__nextId, game_room_size, sudoku)
            self.__nextId += 1
            self.__sessions[session.GetId()] = session
            return session
        finally:
            self.__lock.release()

    def __removeFinishedSessions(self):
        ids = list(self.__getFinishedSessionIds())
        for id in ids:
            del self.__sessions[id]

    def __getFinishedSessionIds(self):
        for id in self.__sessions:
            if self.__sessions[id].IsGameOver():
                yield id
