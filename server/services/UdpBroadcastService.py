import socket
import time


class UdpBroadcastService:
    def __init__(self):
        self.__stopped = False

    def Broadcast(self, broadcast_addr, server_addr):
        msg = ('SudokuServer@%s:%d' % server_addr).encode('utf-8')
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        while not self.__stopped:
            sock.sendto(msg, broadcast_addr)
            time.sleep(1)

    def Stop(self):
        self.__stopped = True
