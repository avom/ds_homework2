import logging
import threading

from argparse import ArgumentParser

from services.RpcServerService import Server
from services.UdpBroadcastService import UdpBroadcastService

FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=FORMAT)
log = logging.getLogger(__name__)


DEFAULT_SERVER_HOST = '127.0.0.1'
DEFAULT_SERVER_PORT = 12345
DEFAULT_BROADCAST_HOST = '255.255.255.255'
DEFAULT_BROADCAST_PORT = 12345

if __name__ == '__main__':
    parser = ArgumentParser(description='Sudoku Server',
                            version='0.1')
    parser.add_argument('-H', '--host',
                        help='Server INET address '
                        'defaults to %s' % DEFAULT_SERVER_HOST,
                        default=DEFAULT_SERVER_HOST)
    parser.add_argument('-P', '--port', type=int,
                        help='Server UDP port, '
                        'defaults to %d' % DEFAULT_SERVER_PORT,
                        default=DEFAULT_SERVER_PORT)
    parser.add_argument('-b', '--broadcast-host',
                        help='UDP broadcast host address, '
                        'defaults to %s' % DEFAULT_BROADCAST_HOST,
                        default=DEFAULT_BROADCAST_HOST)
    parser.add_argument('-p', '--broadcast-port', type=int,
                        help='UDP broadcast host port, '
                        'defaults to %d' % DEFAULT_BROADCAST_PORT,
                        default=DEFAULT_BROADCAST_PORT)
    args = parser.parse_args()

    broadcast_addr = (args.broadcast_host, args.broadcast_port)
    server_addr = (args.host, args.port)
    broadcaster = UdpBroadcastService()
    broadcaster_thread = threading.Thread(target=broadcaster.Broadcast,
                                          args=(broadcast_addr, server_addr, ))
    broadcaster_thread.start()

    try:
        try:
            Server((args.host, args.port)).Start()
        finally:
            broadcaster.Stop()
            broadcaster_thread.join()
            log.info('Server is shutting down.')
    except KeyboardInterrupt:
        pass
    except Exception:
        log.exception('Server stopped due to unhandled error.')
